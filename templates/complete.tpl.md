Reporting - {{ date }}
----------------------

Hits by onionsite:

{% for year in metrics['hits'] %}
  {%- for month in metrics['hits'][year] %}
  {{ year }}-{{ '%02d' % (month) }}:
    {%- for onionsite in metrics['hits'][year][month] %}
      {{ onionsite }}: {{ metrics['hits'][year][month][onionsite] }} hits
    {%- endfor %}
  {%- endfor %}
{%- endfor %}

Hits by onionsite and circuit:

{% for year in metrics['circuits'] %}
  {%- for month in metrics['circuits'][year] %}
  {{ year }}-{{ '%02d' % (month) }}:
    {%- for onionsite in metrics['circuits'][year][month] %}
      {{ onionsite }}:
      {%- for circuit in metrics['circuits'][year][month][onionsite] %}
        {{ circuit }}: {{ metrics['circuits'][year][month][onionsite][circuit] }} circuits
      {%- endfor %}
    {%- endfor %}
  {%- endfor %}
{%- endfor %}

Dates by onionsite:

{% for onionsite in metrics['dates'] %}
  {{ onionsite }}:
    earliest record: {{ metrics['dates'][onionsite]['earliest_record'] }}
    latest record: {{ metrics['dates'][onionsite]['latest_record'] }}
{%- endfor %}

Total:

* Page hit regex: {{ pagehit_regex }}
* Page hits: {{ metrics['matches']['page_hits'] }}
* Non-page hits: {{ metrics['matches']['non_page_hits'] }}
* Ignored User Agents: {{ metrics['matches']['user_agents_ignored'] }}
* Ignored User Agent regex: {{ ignore_user_agent_regex }}

Summary:

{{ summary }}
